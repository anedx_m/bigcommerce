<?php

use Bigcommerce\Api\Client as Bigcommerce;
use Bigcommerce\Api\Filter as Filter;

class BC {

    public static function makeOptionset($optionset_name, $optionset_options) {
        return new BC_Optionset($optionset_name, $optionset_options);
    }

    public static function updateProductSkus($product_id, $skus) {
        $sku_ids = self::getProductSkus($product_id);
//         var_dump($sku_ids);
//         var_dump($skus);
        foreach ($skus as $sku)
            if (isset($sku_ids[$sku['sku']])) {
                self::updateProductSku($product_id, $sku_ids[$sku['sku']], $sku);
            } else {
                var_dump('sku undefined');
//                die();
            }
    }

    public static function getProductSkus($product_id) {
        $r = Bigcommerce::getCollection('/products/' . $product_id . '/skus');
        $product_skus = array();
        if ($r) {
            foreach ($r as $product_sku) {
                $product_skus[$product_sku->sku] = $product_sku->id;
            }
        }
        return $product_skus;
    }

    public static function updateProductSku($product_id, $sku_id, $sku) {
//         var_dump('/products/' . $product_id . '/skus/' . $sku_id);
        Bigcommerce::updateResource('/products/' . $product_id . '/skus/' . $sku_id, $sku);
    }

// add new category if necessary
    public static function getCategoryId($category) {
        $r = Bigcommerce::getCategories(array('name' => $category));
        if ($r) {
            return $r[0]->id;
        } else {
            $r = Bigcommerce::createResource('/categories', array('name' => $category));
            return $r->id;
        }
    }

// add new brand if necessary
    public static function getBrandId($brand) {
        $r = Bigcommerce::getBrands(array('name' => $brand));
        if ($r) {

            return $r[0]->id;
        } else {
            $r = Bigcommerce::createResource('/brands', array('name' => $brand));
            return $r->id;
        }
    }

    public static function getTaxClassId($tax_class) {
        $object = array('name' => $tax_class);
        $filter = Filter::create($object);
        $r = Bigcommerce::getCollection('/tax_classes/' . $filter->toQuery());
        if ($r) {
            return $r[0]->id;
        } else {
            $r = Bigcommerce::createResource('/tax_classes', array('name' => $tax_class));
            return $r->id;
        }
    }

    public static function getProductOptions($product_id) {
        $r = Bigcommerce::getCollection('/products/' . $product_id . '/options');
        $options = array();
        if ($r) {
            foreach ($r as $option) {
                $options[$option->display_name] = $option->id;
            }
        }
//        var_dump('/products/' . $product_id . '/options');
        return $options;
    }

    public static function updateProduct($product_id, $product, $skus = array(), $optionset = null) {
// prepare 
        var_dump($product['name']);
        if (isset($product['brand'])) {
            $product['brand_id'] = self::getBrandId($product['brand']);
            unset($product['brand']);
        }

        if (isset($product['category'])) {
            $product['categories'] = array(self::getCategoryId($product['category']));
            unset($product['category']);
        }

        if (isset($product['tax_class'])) {
            $product['tax_class_id'] = self::getTaxClassId($product['tax_class']);
            unset($product['tax_class']);
        }

        $images = array();

        if (isset($product['image_thumb'])) {
            $images[] = array('image_file' => $product['image_thumb'], 'is_thumbnail' => true);
            unset($product['image_thumb']);
        }
        if (isset($product['image_main'])) {
            $images[] = array('image_file' => $product['image_main'], 'is_thumbnail' => false);
            unset($product['image_main']);
        }
        if (isset($product['images'])) {
            $images = array_merge($images, $product['images']);
            unset($product['images']);
        }
//        var_dump($images);
        $video_url = '';
        if (isset($product['video'])) {
            $video_url = $product['video'];
            unset($product['video']);
        }
        if (empty($product['related_products'])) {
            unset($product['related_products']); //debug
        }
// update 
//        var_dump($product);
        if ($product_id) {
            Bigcommerce::updateProduct($product_id, $product);
        } else {
// debug ---=-=-=-=-=---
            $pr_id = self::getProductId($product['name']);
            $product_id = $pr_id;
            if ($pr_id) {

                Bigcommerce::updateProduct($product_id, $product);
            } else {
                $pr = Bigcommerce::createProduct($product);
                $product_id = $pr->id;
            }
        }

        Bigcommerce::$connection = false;
        Bigcommerce::setCipher('RC4-SHA');
//        var_dump(Bigcommerce::getLastError());
        if ($video_url) {
//https://developer.bigcommerce.com/api/stores/v2/products/videos#delete-product-videos
            Bigcommerce::deleteResource('/products/' . $product_id . '/videos');
//https://developer.bigcommerce.com/api/stores/v2/products/videos#create-product-video
            $object = array(
                'url' => $video_url,
            );
            Bigcommerce::createResource('/products/' . $product_id . '/videos', $object);
        }
//        die();
        if ($images) {
//https://developer.bigcommerce.com/api/stores/v2/products/images#delete-product-images
            $i = Bigcommerce::getProductImages($product_id);
            if ($i) {
                Bigcommerce::deleteResource('/products/' . $product_id . '/images');
            }
            foreach ($images as $image) {
//                 var_dump($image);
                Bigcommerce::createResource('/products/' . $product_id . '/images', $image);
            }
        }

//SKU
//получить опции продукта https://developer.bigcommerce.com/api/stores/v2/products/options как массив Name=>Id
//for example $product_options = array("Color"=>12,"Size"=>14);
        $product_options = self::getProductOptions($product_id);

        foreach ($skus as $sku) {
//            var_dump($sku);
            $options = array();
            foreach ($sku['options'] as $option => $value) {
                $opt = new stdClass;
                $opt->product_option_id = $product_options[$option];
                $opt->option_value_id = $optionset->getOptionValueId($option, $value);
                $options[] = $opt;
            }
            $sku['options'] = $options;

            $r = self::getProductSku($product_id, $sku['sku']);
//            var_dump($r,  Bigcommerce::getLastError());

            if ($r) {
//                 var_dump('/products/' . $product_id . '/skus/' . $r->id);
                Bigcommerce::updateResource('/products/' . $product_id . '/skus/' . $r->id, $sku);
            } else {
                $y = Bigcommerce::createResource('/products/' . $product_id . '/skus', $sku);
//                var_dump($y,  Bigcommerce::getLastError());
            }
        }
        return $product_id;
    }

    private static function getProductSku($product_id, $sku) {
        $object = array('sku' => $sku);
        $filter = Filter::create($object);
        $r = Bigcommerce::getCollection('/products/' . $product_id . '/skus/' . $filter->toQuery());
        if ($r) {
            return $r[0];
        } else {
            return false;
        }
    }

    private static function getProductId($product_name) {
        $r = Bigcommerce::getProducts(array('name' => $product_name));
        if ($r) {
            return $r[0]->id;
        }
        return false;
    }
    
    public static function getOrders ($filter) {
		$orders = Bigcommerce::getOrders($filter);
		if($orders) {
			foreach($orders as $pos=>$order) {
				$orders[$pos]->shipments = $order->shipments();
				$orders[$pos]->products = $order->products();
				$orders[$pos]->shipping_addresses = $order->shipping_addresses();
				$orders[$pos]->coupons = $order->coupons();
			}
		}
		return $orders;
	}
	
}
?>