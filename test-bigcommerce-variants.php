<?php
$product_id = 0; //we will ADD
$related_products = array();  // as list by ID i.e. (12223,2324233,343444)
//N - Numbers only text D - Date MT - Multiline text C - Checkbox F - File T - Text RB - Radio list RT - Rectangle list S - Select box P - Product list PI - Product list with images CS - Swatch
$product_options = array(
    array('Name' => 'Color', 'Type' => 'S', 'Values' => array('red', 'blue')),
    array('Name' => 'Size', 'Type' => 'S', 'Values' => array('small', 'large'))
);
// will create object with options,optionvalues,optionset
// will add/remove values for existing options
$optionset = BC::makeOptionset("Color and Size", $product_options);  // first parameter -- optionset name

$product = array();
$product['name'] = 'Train Starter Set';
$product['description'] = 'The Train Starter Set (LEGO-No. 10507) is a fantastic entry level toy for children.';
$product['type'] = 'physical'; // physical or digital 
$product['weight'] = 0.5;
$product['price'] = 100;
$product['availability'] = 'available';
// none - inventory levels will not be tracked. simple - inventory levels will be tracked using the "inventory_level" and "inventory_warning_level" fields. sku - inventory levels will be tracked based on individual product options 
$product['option_set_id'] = $optionset->ID;
$product['inventory_tracking'] = 'sku';
$product['related_products'] = $related_products;

// next fields are resources !
$product['brand'] = 'LEGO'; //* only name - unique
$product['category'] = 'DUPLO'; //* only name - unique?
/* // we can pass images as array  https://developer.bigcommerce.com/api/objects/v2/product_image 
$product['images'] = array(
    array(
        'image_file' => 'http://www.online-image-editor.com/styles/2013/images/example_image.png',
        'is_thumbnail' => false,
        'description' => 'cat'
    ),
    array(
        'image_file' => 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRre3qu9bttTrkI1hMScmjFWpzJG5Xrs0xvVk_f3hfc_CMnuIwP',
        'is_thumbnail' => false,
        'description' => 'cat'
    ),
);
*/
$product['image_main'] = "http://images.mytoys.com/intershoproot/eCS/Store/de/images/314/54/3145436-4-x.jpg"; // *
$product['image_thumb'] = "http://images.mytoys.com/intershoproot/eCS/Store/de/images/314/54/3145436-u.gif"; // * 
$product['video'] = "http://www.youtube.com/watch?v=W8dCP4edJn4"; // * 
$product['option_set_id'] = $optionset->ID;
$product['tax_class'] = 'default'; //values: default for 'Default Tax Class',notax for 'Non-=Taxable Product', shipping for 'Shipping', wrapping for 'Gift Wrapping')

$skus = array(
    array('sku' => "10507", 'cost_price' => 39.99, 'inventory_level' => '15', 'inventory_warning_level' => '5', 'options' => array('Color' => 'red', 'Size' => 'small')),
    array('sku' => "10508", 'cost_price' => 59.99, 'inventory_level' => '15', 'inventory_warning_level' => '5', 'options' => array('Color' => 'blue', 'Size' => 'large')),
);

//1 add/update product 

$product_id = BC::updateProduct($product_id, $product, $skus, $optionset);
//var_dump($product_id);
//die();
//2 set price
$skus = array(
    array('sku' => "10507", 'cost_price' => 37.99),
    array('sku' => "10508", 'cost_price' => 57.99),
);
BC::updateProductSkus($product_id, $skus);

//3 set stock
$skus = array(
    array('sku' => "10507", 'inventory_level' => '8'),
    array('sku' => "10508", 'inventory_level' => '7'),
);
BC::updateProductSkus($product_id, $skus);
