<?php

use Bigcommerce\Api\Client as Bigcommerce;

class BC_Optionset {

    var $ID = 0;
    var $value_id;

    function __construct($name, $options) {

        $this->ID = $this->getOptionsetId($name);
//        var_dump('id', $this->ID);
        //  https://developer.bigcommerce.com/api/stores/v2/option_sets/options#list-option-set-options  as array name=>id
        $current_options = $this->getOptions();
//        var_dump($current_options);
//        die();
        $this->value_id = array();
        foreach ($options as $option) {
            $option_name = $option['Name'];
            $option_id = $this->getOptionId($option_name, $option['Type']);
//            var_dump($option_id);
            $this->value_id[] = array();
            // https://developer.bigcommerce.com/api/stores/v2/options/values#list-option-values as array $value=>id
            // for example, 
            $current_values = $this->getOptionValues($option_id);

            foreach ($option['Values'] as $value) {
                //$option_value_id = https://developer.bigcommerce.com/api/stores/v2/options/values#create-option-value
                if (!isset($current_values[$value])) {
                    $object = array(
                        'value' => $value,
                        'label' => $value
                    );
                    $r = Bigcommerce::createResource('/options/' . $option_id . '/values', $object);
                    if ($r) {
                        $option_value_id = $r->id;
                    } else {
                        var_dump(Bigcommerce::getLastError());
                        die();
                    }
                } else {
                    $option_value_id = $current_values[$value];
                    unset($current_values[$value]);
                }
                $this->value_id[$option_name][$value] = $option_value_id;
            }


            if (!isset($current_options[$option_name])) {
                // $this->ID + $option_id   https://developer.bigcommerce.com/api/stores/v2/option_sets/options#create-option-set-option
                $object = array(
                    'option_id' => $option_id,
                );
                Bigcommerce::createResource('/option_sets/' . $this->ID . '/options', $object);
            } else {
                unset($current_options[$option_name]); // in usage 
            }
//            var_dump($current_options);
            // remove unused options from our optionset
        }
        foreach ($current_options as $outdated_option_name => $outdated_id) {
//            echo '$outdated_id = ';
//            var_dump($outdated_id);
            $r = Bigcommerce::deleteResource('/option_sets/' . $outdated_id . '/options');
//            var_dump($r);
        }
    }

    function getOptionsetId($name) {
        $r = Bigcommerce::getOptionSets(array('name' => $name));
        if ($r) {
            return $r[0]->id;
        } else {
            $op_set = array(
                'name' => $name,
            );
            $r = Bigcommerce::createOptionsets($op_set);
            return $r->id;
        }
    }

    function getOptionId($name, $type) {
        $r = Bigcommerce::getOptions(array('name' => $name, 'type' => $type));
        if ($r) {
            return $r[0]->id;
        } else {
            $op = array(
                'name' => $name,
                'type' => $type,
            );
            $r = Bigcommerce::createOptions($op);
            return $r->id;
        }
    }

//
    function getOptionValues($option_id) {
        $r = Bigcommerce::getCollection('/options/' . $option_id . '/values');
        $values = array();
        if ($r) {
            foreach ($r as $option) {
                $values[$option->value] = $option->id;
            }
        }
        return $values;
    }

    function getOptions() {
        $r = Bigcommerce::getCollection('/option_sets/' . $this->ID . '/options');
//        var_dump($r);
        $options = array();
        if ($r) {
            foreach ($r as $option) {
                $options[$option->display_name] = $option->id;
            }
        }
        return $options;
    }

    function getOptionValueId($option, $value) {
        if (!isset($this->value_id[$option][$value])) {
            throw new Excepton("Undefined value '$value' for option '$option'");
        }

        return $this->value_id[$option][$value];
    }

}
