<?php
$product_id =0; //we will ADD
$related_products = array();  // as list by ID i.e. (12223,2324233,343444)

$product = array();
$product['name'] = 'Train Starter Set';
$product['description'] = 'The Train Starter Set (LEGO-No. 10507) is a fantastic entry level toy for children.';
$product['sku'] = 10507;
$product['price'] = '39.99';
$product['type'] = 'physical'; // physical or digital 
$product['weight'] = 0.5;
$product['availability'] = 'available';
// none - inventory levels will not be tracked. simple - inventory levels will be tracked using the "inventory_level" and "inventory_warning_level" fields. sku - inventory levels will be tracked based on individual product options 
$product['inventory_tracking'] = 'simple'; 
$product['inventory_level'] = '15';//ignored in sku mode
$product['inventory_warning_level'] = '5';//ignored in sku mode
$product['related_products'] = $related_products;

// next fields are resources !
$product['brand'] = 'LEGO'; //* only name - unique
$product['category'] = 'DUPLO'; //* only name - unique?
$product['image_main'] = "http://images.mytoys.com/intershoproot/eCS/Store/de/images/314/54/3145436-4-x.jpg";// *
$product['image_thumb'] = "http://images.mytoys.com/intershoproot/eCS/Store/de/images/314/54/3145436-u.gif";// * 
$product['video'] = "http://www.youtube.com/watch?v=W8dCP4edJn4"; // * 
$product['tax_class'] = 'default'; //values: default for 'Default Tax Class',notax for 'Non-=Taxable Product', shipping for 'Shipping', wrapping for 'Gift Wrapping')

//1 add/update product 
$product_id = BC::updateProduct($product_id,$product);

//2 set price
$product = array('price' => '37.99');
$product['name'] = 'Train Starter Set';
BC::updateProduct($product_id,$product);

//3 set stock
$product = array('inventory_level' => 8);
$product['name'] = 'Train Starter Set';
BC::updateProduct($product_id,$product);