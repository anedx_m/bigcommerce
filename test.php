<?php

require 'bigcommerce.php';
require 'BC_Optionset.php';
require 'BC.php';

use Bigcommerce\Api\Client as Bigcommerce;
use Bigcommerce\Api\Resources as Resources;

Bigcommerce::configure(array(
    'store_url' => 'https://store-bje5q.mybigcommerce.com',
    'username' => 'wizardhq',
    'api_key' => 'a5ee9fd1b6617df479f683a9690c544f171425c9'
));
//Bigcommerce::configure(array(
//    'store_url' => 'https://store-gcb6nc3w.mybigcommerce.com',
//    'username' => 'anedx',
//    'api_key' => 'c152b956e6bda6e786e61aec956a872207d17ae7'
//));

Bigcommerce::setCipher('RC4-SHA');

$ping = Bigcommerce::getTime();

if ($ping) {
    Bigcommerce::failOnError(true);
//    var_dump(Bigcommerce::getCategories());
//    include 'test-bigcommerce-variants.php';
//    include 'test-bigcommerce-simple.php';
    include 'test-get-last-orders.php';
} else {
    echo 'not connected';
}